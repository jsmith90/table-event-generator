import TableDisplay from "./components/table/TableDisplay";
import FormGenerator from "./components/form/FormGenerator";
import { Button, Dialog } from "@mui/material";
import useTable, { TableContext } from "./components/table/useTable";
import { Box } from "@mui/system";
import Layout from "./components/layout/Layout";

function App() {
  const useTableHook = useTable();
  const { overlay, setOverlay } = useTableHook;

  return (
    <Layout>
      <TableContext.Provider value={useTableHook}>
        <section aria-labelledby="createEvent">
          <Box component="div" className="createEventBox">
            <Button
              id="createEvent"
              variant="contained"
              onClick={() => setOverlay(true)}
            >
              Create Event
            </Button>
          </Box>
        </section>
        <section aria-labelledby="tableContent">
          <div>
            <TableDisplay />
          </div>
        </section>

        <Dialog open={overlay} fullWidth className="overlay">
          <Button onClick={() => setOverlay(false)}>x</Button>
          <h2>Create event</h2>
          <FormGenerator />
        </Dialog>
      </TableContext.Provider>
    </Layout>
  );
}

export default App;

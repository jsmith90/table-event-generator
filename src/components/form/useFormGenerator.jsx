import { useState, useEffect, useContext } from "react";
import jsonForm from "./jsonForm.json";

import { useForm } from "react-hook-form";
import axios from "axios";
import { TableContext } from "../table/useTable";

const dataURL = "http://localhost:3004/data";

function useFormGenerator() {
  const [elements, setElements] = useState([]);
  const { setRows, rows } = useContext(TableContext);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  useEffect(() => {
    setElements(jsonForm);
  }, []);

  const onSubmit = async (data) => {
    // const resp = await axios.post(dataURL, { newRowData });
    // console.log(resp);
    const newIdRow = { id: rows[rows.length - 1][0]++, ...data };
    const newRowData = Object.values(newIdRow);
    setRows((state) => [...state, newRowData]);
  };

  return {
    register,
    elements,
    handleSubmit,
    onSubmit,
  };
}

export default useFormGenerator;

import React from "react";
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextareaAutosize,
  Button,
  TextField,
} from "@mui/material";
import useFormGenerator from "./useFormGenerator";
import "./FormGenerator.scss";

function FormGenerator() {
  const { handleSubmit, onSubmit, register, elements } = useFormGenerator();

  function RenderForm() {
    return elements.map((val) =>
      val.component === "text" ? (
        <FormControl className="formControl" fullWidth key={val.label}>
          <TextField
            label={val.label}
            {...register(val.label.toLowerCase())}
            variant="outlined"
          />
        </FormControl>
      ) : val.component === "select" ? (
        <FormControl className="formControl" fullWidth key={val.label}>
          <Select
            displayEmpty
            defaultValue=""
            {...register(val.label.toLowerCase())}
          >
            {val.options.map((option) => {
              return (
                <MenuItem key={option.label} value={option.value}>
                  {option.label}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
      ) : val.component === "range_picker" ? (
        <div key={val.label}>
          <FormControl className="formControl dateRange">
            {val.name.map((date) => (
              <div>
                <TextField
                  label={date}
                  {...register(val.label.toLowerCase())}
                  variant="outlined"
                />
              </div>
            ))}
          </FormControl>
        </div>
      ) : (
        val.component === "textarea" && (
          <div key={val.label}>
            <InputLabel>{val.label}</InputLabel>
            <TextareaAutosize
              key={val.label}
              aria-label={val.label}
              {...register(val.label.toLowerCase())}
            />
          </div>
        )
      )
    );
  }
  return (
    <form className="createEventForm" onSubmit={handleSubmit(onSubmit)}>
      <RenderForm />
      <Button type="submit">Submit</Button>
    </form>
  );
}

export default FormGenerator;

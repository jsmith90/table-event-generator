import React from "react";
import logo from "./logo.svg";
import "./Layout.scss";
import { Container } from "@mui/system";

function Layout({ children }) {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} alt="Logo" width="100" />
      </header>
      <main>
        <Container maxWidth="xl">{children}</Container>
      </main>
    </div>
  );
}

export default Layout;

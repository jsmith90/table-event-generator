import { createContext, useEffect, useState } from "react";
import axios from "axios";

const dataURL = "http://localhost:3004/data";

async function tableDataApi() {
  try {
    const res = await axios.get(dataURL);

    return res.data;
  } catch (err) {
    console.error(err);
  }
}

async function tableQuery() {
  const data = await tableDataApi();

  const tableHeaders = Object.keys(data[0]);
  const rows = data.map((row) => {
    return Object.values(row);
  });

  return {
    tableHeaders,
    rows,
  };
}

const useTable = () => {
  const [headers, setHeaders] = useState([]);
  const [rows, setRows] = useState([]);
  const [overlay, setOverlay] = useState(false);

  async function data() {
    const res = await tableQuery();
    setHeaders(res.tableHeaders);
    setRows(res.rows);
    return res;
  }

  useEffect(() => {
    data();
  }, []);
  return {
    headers,
    rows,
    setRows,
    overlay,
    setOverlay,
  };
};
export default useTable;

export const TableContext = createContext({});

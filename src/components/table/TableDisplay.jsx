import React, { useContext, useEffect, useState } from "react";
import axios from "axios";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@mui/material";
import useTable, { TableContext } from "./useTable";

const TableDisplay = () => {
  // const { headers, rows } = useTable();
  const { headers, rows, setRows } = useContext(TableContext);

  return (
    <Table id="tableContent">
      <TableHead>
        <TableRow>
          {headers?.map((trow) => (
            <TableCell key={trow}>{trow}</TableCell>
          ))}
        </TableRow>
      </TableHead>
      <TableBody>
        {rows.map((row) => (
          <TableRow key={row[0]}>
            {row.map((rowValue) => (
              <TableCell key={rowValue}>{rowValue}</TableCell>
            ))}
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
};

export default TableDisplay;
